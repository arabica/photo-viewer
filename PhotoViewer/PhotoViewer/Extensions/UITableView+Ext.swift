//
//  UITableView+Ext.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/27/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import UIKit

extension UITableView {
    func register<T: UITableViewCell>(_:T.Type) {
        self.register(T.nib, forCellReuseIdentifier: T.identifier)
    }
}

