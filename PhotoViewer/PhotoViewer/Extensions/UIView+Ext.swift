//
//  UIView+Ext.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/25/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import UIKit

protocol NibInstance where Self: UIView {
    static func instance() -> Self
    static var nib: UINib { get }
    static var identifier: String { get }
}

extension NibInstance {
    static func instance() -> Self {
        let instance = Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)?.first as? Self
        return instance!
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: Bundle.main)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: NibInstance {}
extension UITableViewHeaderFooterView: NibInstance {}
extension UICollectionReusableView: NibInstance {}

extension UIView {
    
    func createImage() -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0)
        drawHierarchy(in: frame, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
