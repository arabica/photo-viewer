//
//  Dictionary+Ext.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/23/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import Foundation

extension Dictionary {
    func appendingWithReplacing(_ other: Dictionary) -> Dictionary {
        return self.merging(other) { _, new in new }
    }
    
    mutating func appendWithReplace(_ other: Dictionary) {
        self.merge(other) { _, new in new }
    }
}
