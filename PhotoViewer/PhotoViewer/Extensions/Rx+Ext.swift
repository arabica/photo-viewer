//
//  Rx+Ext.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/17/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import RxSwift

extension Observable {
    func mapToVoid() -> Observable<Void> {
        return self.map { _ in () }
    }
}
