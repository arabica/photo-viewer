//
//  UIButton+Ext.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/27/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import UIKit

extension UIButton {
    func makeImageTemplate() -> UIButton {
        let image = self.image(for: .normal)?.withRenderingMode(.alwaysTemplate)
        self.setImage(image, for: .normal)
        return self
    }
}
