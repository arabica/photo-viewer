//
//  UIImage.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/26/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import UIKit
import Accelerate

extension UIImage {
    var averageColor: UIColor {
        let numberOfPixels = Int(self.size.width * self.size.height)
        guard numberOfPixels > 0, let cg = self.cgImage, let provider = cg.dataProvider else { return .clear }
        
        var greenVector:[Float] = Array(repeating: 0.0, count: numberOfPixels)
        var blueVector:[Float] = Array(repeating: 0.0, count: numberOfPixels)
        var redVector:[Float] = Array(repeating: 0.0, count: numberOfPixels)
        
        let pixelData = provider.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        
        vDSP_vfltu8(data, 4, &blueVector, 1, vDSP_Length(numberOfPixels))
        vDSP_vfltu8(data+1, 4, &greenVector, 1, vDSP_Length(numberOfPixels))
        vDSP_vfltu8(data+2, 4, &redVector, 1, vDSP_Length(numberOfPixels))
        
        // compute average per color
        var redAverage:Float = 0.0
        var blueAverage:Float = 0.0
        var greenAverage:Float = 0.0
        
        vDSP_meamgv(&redVector, 1, &redAverage, vDSP_Length(numberOfPixels))
        vDSP_meamgv(&greenVector, 1, &greenAverage, vDSP_Length(numberOfPixels))
        vDSP_meamgv(&blueVector, 1, &blueAverage, vDSP_Length(numberOfPixels))
        
        // convert to HSV ( hue, saturation, value )
        // this gives faster, more accurate answer
        var hue: CGFloat = 0.0
        var saturation: CGFloat = 0.0
        var brightness: CGFloat = 0.0
        var alpha: CGFloat = 1.0
        
        let color: UIColor = UIColor(red: CGFloat(redAverage/255.0), green: CGFloat(greenAverage/255.0), blue: CGFloat(blueAverage/255.0), alpha: alpha)
        color.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        
        return color
    }
}
