//
//  String+Ext.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/22/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import Foundation
import CommonCrypto

extension String {
    
    func sha1(secretKey key: String) -> String {
        let str = Array(self.utf8CString)
        let digestLen = Int(CC_SHA1_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<UInt8>.allocate(capacity: digestLen)
        defer { result.deallocate() }
        
        key.data(using: .utf8)?.withUnsafeBytes { body in
            CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA1), UnsafeRawPointer(body), key.count, str, count, result)
        }
        
        return (0..<digestLen).map {
            let s = String(result[$0], radix: 16)
            return s.count % 2 == 0 ? s : "0" + s
            }.joined()
    }
    
}
