//
//  FlickrAPIMethod.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/23/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import Foundation

protocol FlickrAPIMethod: CustomStringConvertible { }

struct FlickrMethod {
    static let id = "flickr"
    
    enum Photos: String, FlickrAPIMethod {
        static var id: String {
            return "\(FlickrMethod.id).photos"
        }
        
        case getInfo, recentlyUpdated, removeTag, search, setContentType, setDates, setMeta, setPerms, setSafetyLevel, setTags
        
        var description: String {
            return "\(Photos.id).\(self.rawValue)"
        }
        
        enum Comments: String, FlickrAPIMethod {
            static var id: String {
                return "\(Photos.id).comments"
            }
            
            case addComment, deleteComment, editComment, getList, getRecentForContacts
            
            var description: String {
                return "\(Comments.id).\(self.rawValue)"
            }
        }
    }
}
