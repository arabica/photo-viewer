//
//  OAuthService.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/17/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import OAuthSwift
import RxSwift
import KeychainSwift
import Alamofire

private extension Dictionary {
    var sortedParameterString: String {
        return self.map { "\($0)=\($1)" }
            .sorted()
            .joined(separator: "&")
    }
}

private struct URLEncoder: ParameterEncoding {
    private enum Errors: Error {
        case badRequest, cantSignRequest
    }
    
    private struct Keys {
        static let nonce = "oauth_nonce"
        static let timestamp = "oauth_timestamp"
        static let consumerKey = "oauth_consumer_key"
        static let signatureMethod = "oauth_signature_method"
        static let version = "oauth_version"
        static let token = "oauth_token"
        static let signature = "api_sig"
    }
    
    private let authToken: String
    private let authTokenSecret: String
    private let consumerKey: String
    private let consumerSecret: String
    private let nonce: UUID
    private let timestamp: TimeInterval
    
    init(authToken: String, authTokenSecret: String, consumerKey: String, consumerSecret: String, nonce: UUID, timestamp: TimeInterval) {
        self.authToken = authToken
        self.authTokenSecret = authTokenSecret
        self.consumerKey = consumerKey
        self.consumerSecret = consumerSecret
        self.nonce = nonce
        self.timestamp = timestamp
    }
    
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        guard var request = urlRequest.urlRequest,
            let method = request.httpMethod,
            let url = urlRequest.urlRequest?.url?.absoluteString else {
                throw Errors.badRequest
        }
        
        let params = parameters?
            .mapValues { "\($0)" }
            .appendingWithReplacing(self.baseRequestParams)
            ?? self.baseRequestParams
        
        let paramsString = params.sortedParameterString
        
        guard let signature = (method + [url, paramsString].joined(separator: "?"))
            .addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)?
            .sha1(secretKey: "\(self.consumerSecret)&\(self.authTokenSecret)") else {
                throw Errors.cantSignRequest
        }
        
        let signedParams = params.appendingWithReplacing([Keys.signature : signature])
        
        let signedUrl = [url, signedParams.sortedParameterString]
            .joined(separator: "?")
        request.url = URL(string: signedUrl)
        
        return request
    }
    
    private var baseRequestParams: [String : String] {
        return [
            Keys.nonce: self.nonce.uuidString,
            Keys.timestamp: "\(Int(self.timestamp))",
            Keys.consumerKey: consumerKey,
            Keys.signatureMethod: "HMAC-SHA1",
            Keys.version: "1.0",
            Keys.token: self.authToken
        ]
    }
}

struct ConsumerKeys {
    let consumerKey: String
    let consumerSecret: String
    
    static let flickerKeys = ConsumerKeys(consumerKey: "2f4917fa18d1d9373589185b3b9e032a", consumerSecret: "ca702c762fdd3b8a")
}

struct OAuthURLs {
    let requestTokenUrl: String
    let authorizeUrl: String
    let accessTokenUrl: String
    let redirectURL: String
    
    static let flickerURLs = OAuthURLs(
        requestTokenUrl: "https://www.flickr.com/services/oauth/request_token",
        authorizeUrl: "https://www.flickr.com/services/oauth/authorize?perms=write",
        accessTokenUrl: "https://www.flickr.com/services/oauth/access_token",
        redirectURL: "oauth-photoviewer://oauth-callback/flickr"
    )
}

class AuthClient {
    private struct Keys {
        static let authToken = "auth_token"
        static let authTokenSecret = "auth_token_secret"
    }
    
    enum State {
        case logedIn
        case notAuthorized
    }
    
    enum Errors: Error {
        case emptyToken
    }
    
    private (set) var  state: State
    
    private let oauth: OAuth1Swift
    private let keychain: KeychainSwift
    private let consumerKeys: ConsumerKeys
    private let oauthUrls: OAuthURLs
    private var credentials: OAuthSwiftCredential?
    
    init(keychain: KeychainSwift = KeychainSwift(), consumerKeys: ConsumerKeys = .flickerKeys, oauthUrls: OAuthURLs = .flickerURLs) {
        self.consumerKeys = consumerKeys
        self.oauthUrls = oauthUrls
        
        self.oauth = OAuth1Swift(
            consumerKey: consumerKeys.consumerKey,
            consumerSecret: consumerKeys.consumerSecret,
            requestTokenUrl: oauthUrls.requestTokenUrl,
            authorizeUrl: oauthUrls.authorizeUrl,
            accessTokenUrl: oauthUrls.accessTokenUrl
        )
        
        self.keychain = keychain
        if let token = keychain.get(Keys.authToken),
            let tokenSecret = keychain.get(Keys.authTokenSecret){
            self.credentials = OAuthSwiftCredential(consumerKey: self.consumerKeys.consumerKey, consumerSecret: self.consumerKeys.consumerSecret)
            self.credentials?.oauthToken = token
            self.credentials?.oauthTokenSecret = tokenSecret
            
            self.state = .logedIn
        } else {
            self.state = .notAuthorized
        }
    }
    
    func authorize() -> Observable<String> {
        return Observable<String>.create { observer in
            self.oauth.authorize(
                withCallbackURL: self.oauthUrls.redirectURL,
                success: { (credentials, response, parameters) in
                    self.write(credentials: credentials, to: self.keychain)
                    self.credentials = credentials
                    self.state = .logedIn
                    
                    observer.onNext(credentials.oauthToken as String)
                    observer.onCompleted()
                }, failure: { (error) in
                    observer.onError(error)
                }
            )
            
            return Disposables.create()
        }
    }
    
    func parameterEncoder(with nonce: UUID = UUID(), timestamp: TimeInterval = Date().timeIntervalSince1970) throws -> ParameterEncoding {
        guard let token = self.credentials?.oauthToken,
            let tokenSecret = self.credentials?.oauthTokenSecret else {
                throw Errors.emptyToken
        }
        
        return URLEncoder(
            authToken: token,
            authTokenSecret: tokenSecret,
            consumerKey: consumerKeys.consumerKey,
            consumerSecret: consumerKeys.consumerSecret,
            nonce: nonce,
            timestamp: timestamp
        )
    }
}

extension AuthClient {
    private func write(credentials: OAuthSwiftCredential, to keychain: KeychainSwift) {
        keychain.set(credentials.oauthToken, forKey: Keys.authToken)
        keychain.set(credentials.oauthTokenSecret, forKey: Keys.authTokenSecret)
    }
}
