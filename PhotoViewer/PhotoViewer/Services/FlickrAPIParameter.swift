//
//  FlickrAPIParameter.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/23/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import Foundation

protocol FlickrAPIParameter {
    var dictionary: [String : String] { get }
}

struct GeneralFlickrParam: FlickrAPIParameter {
    let key: String
    let value: String
    
    init(_ key: String, _ value: String) {
        self.key = key
        self.value = value
    }
    
    var dictionary: [String : String] {
        return [key : value]
    }
}

struct PhotoSearch {
    struct Extras: FlickrAPIParameter {
        enum Params: String {
            case size = "o_dims"
            case description, license
            case dateUpload = "date_upload"
            case ownerName = "owner_name"
            case countFaves = "count_faves"
        }
        
        let params: [Params]
        
        var dictionary: [String : String] {
            return [
                "extras" : self.params.map { $0.rawValue }.joined(separator: ",")
            ]
        }
    }
    
    struct Location: FlickrAPIParameter {
        enum Units: String {
            case miles = "m"
            case killometers = "km"
        }
        
        let coordinates: (lat: Double, lon: Double)
        let radius: Float
        let units: Units
        
        var dictionary: [String : String] {
            return [
                "lat" : "\(coordinates.lat)",
                "lon" : "\(coordinates.lon)",
                "radius" : "\(radius)",
                "radius_units" : "\(units.rawValue)"
            ]
        }
        
    }
    
    struct Sort: FlickrAPIParameter {
        enum SortParam: String {
            case datePostedAsc = "date-posted-asc"
            case datePostedDesc = "date-posted-desc"
            case dateTakenAsc = "date-taken-asc"
            case dateTakenDesc = "date-taken-desc"
            case interestingnessDesc = "interestingness-desc"
            case interestingnessAsc = "interestingness-asc"
            case relevance
        }
        
        let sortParam: SortParam
        
        var dictionary: [String : String] {
            return [ "sort" : sortParam.rawValue ]
        }
    }
    
    
}
