//
//  File.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/16/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import Alamofire
import CodableAlamofire
import RxSwift

class FlickrAPIService {
    typealias FlickrOAuthMethodParams = [String:String]
    
    let baseURL: URLConvertible = "https://api.flickr.com/services/rest/"
    let oauthClient: AuthClient
    
    init(oauthClient: AuthClient = AuthClient()) {
        self.oauthClient = oauthClient
    }
    
    func authorizedRequest<T: FlickrResponse>(method: FlickrAPIMethod, params: [FlickrAPIParameter]) -> Observable<T> {
        let requestParams = [
            "method" : method.description,
            "format" : "json",
            "nojsoncallback" : "1"
            ].appendingWithReplacing(
                params
                    .map { $0.dictionary }
                    .reduce([String : String]()) { $0.appendingWithReplacing($1) }
        )
        
        return Observable.create { [weak self] (observer) -> Disposable in
            guard let `self` = self else {
                observer.onCompleted()
                return Disposables.create()
            }
            
            var parameterEncoding: ParameterEncoding
            do {
                parameterEncoding = try self.oauthClient.parameterEncoder()
            } catch let error {
                observer.onError(error)
                observer.onCompleted()
                return Disposables.create()
            }
            
            Alamofire
                .request(self.baseURL, method: .get, parameters: requestParams, encoding: parameterEncoding)
                .responseDecodableObject(
                    completionHandler: { (response: DataResponse<T>) in
                        switch response.result {
                        case .success(let data):
                            observer.onNext(data)
                        case .failure(let error):
                            observer.onError(error)
                        }
                        observer.onCompleted()
                    }
                )
            
            return Disposables.create()
        }
    }
}
