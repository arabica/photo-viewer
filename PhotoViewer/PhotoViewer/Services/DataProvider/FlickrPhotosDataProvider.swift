//
//  FlickrPhotosDataProvider.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/23/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import RxSwift

class FlickrPhotoDataProvider {
    enum Errors: Error {
        case responseFailed(String?)
        
        var localizedDescription: String {
            switch self {
            case .responseFailed(let message):
                return message ?? "Something went wrong"
            }
        }
    }
    
    private let apiService: FlickrAPIService
    
    init(apiService: FlickrAPIService = FlickrAPIService()) {
        self.apiService = apiService
    }
    
    func search(text: String? = nil) -> Observable<[FlickrPhoto]> {
        let photoRequest: Observable<FlickerPhotoSearchResponse> = self.apiService.authorizedRequest(method: FlickrMethod.Photos.search, params: [
                PhotoSearch.Sort(sortParam: .interestingnessDesc),
                PhotoSearch.Extras(params: [.size, .description]),
//                GeneralFlickrParam("user_id", "me")
            ])
        
        return photoRequest.filter {
                guard case .ok = $0.stat else { throw Errors.responseFailed($0.message) }
                return true
            }.map {
                $0.photos.photo ?? []
            }
    }
    
    func photoInfo(photo: FlickrPhoto) -> Observable<FlickrPhotoInfoModel> {
        let request: Observable<FlickrPhotoInfoResponse> = self.apiService.authorizedRequest(method: FlickrMethod.Photos.getInfo, params: [
                GeneralFlickrParam("photo_id", photo.id),
                GeneralFlickrParam("secret", photo.secret),
                PhotoSearch.Extras(params: [.countFaves])
            ])
        
        return request.filter {
                guard case .ok = $0.stat else { throw Errors.responseFailed($0.message) }
                return true
            }.map {
                $0.photo
            }
    }
    
    func photoComments(_ photo: FlickrPhoto) -> Observable<[FlickrPhotoComment]> {
        let request: Observable<FlickrPhotoCommentResponse> = self.apiService.authorizedRequest(method: FlickrMethod.Photos.Comments.getList, params: [
                GeneralFlickrParam("photo_id", photo.id)
            ])
        
        return request.filter {
                guard case .ok = $0.stat else { throw Errors.responseFailed($0.message) }
                return true
            }.map {
                $0.comments.comment ?? []
            }
    }
}
