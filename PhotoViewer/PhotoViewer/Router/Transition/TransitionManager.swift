//
//  TransitionManager.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/26/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import UIKit
import UIImageColors

enum TransitionType {
    case presentation
    case dismissal
    
    func toggle() -> TransitionType {
        return self == .presentation ? .dismissal : .presentation
    }
    
    var blurAlpha: CGFloat {
        return self == .presentation ? 1 : 0
    }
    
    var cornerRadius: CGFloat {
        return self == .presentation ? 16 : 0
    }
}

class TransitionManager: NSObject, UIViewControllerAnimatedTransitioning {
    private let transitionDuration: Double = 0.75
    private var transition: TransitionType = .presentation
    
    var getPresentData: (() -> (UIImage?, String?, CGRect?))?
    
    private let blurEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        return UIVisualEffectView(effect: blurEffect)
    }()
    
    private let dimmingView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }()
    
    let screenWidth: CGFloat = UIDevice.current.userInterfaceIdiom == .pad ? 700 : UIScreen.main.bounds.width
    private let topPadding: CGFloat = UIDevice.current.userInterfaceIdiom == .pad ? 40 : 0
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return transitionDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        transitionContext.containerView.subviews.forEach { $0.removeFromSuperview() }
        guard let fromVC = transitionContext.viewController(forKey: .from),
            let toVC = transitionContext.viewController(forKey: .to),
            let data = self.getPresentData else {
                transitionContext.completeTransition(false)
                return
        }
            
        let containerView = transitionContext.containerView
        
        let (image, title, frame) = data()
        let titledView = TitledPhotoView.instance()
        titledView.setImage(image: image)
        titledView.title.text = title
        let color: UIColor = image?.getColors().background ?? .clear
        
        switch self.transition {
        case .presentation:
            self.present(fromVC: fromVC, toVC: toVC, containerView: containerView, headerView: titledView, initialFrame: frame ?? .zero, color: color, completion: transitionContext.completeTransition)
        case .dismissal:
            transitionContext.completeTransition(true)
        }
        
    }
    
}

extension TransitionManager {
    private func present(fromVC: UIViewController, toVC: UIViewController, containerView: UIView, headerView: TitledPhotoView, initialFrame: CGRect, color: UIColor, completion: @escaping (Bool) -> Void) {
        
        fromVC.view.snapshotView(afterScreenUpdates: true).flatMap { containerView.addSubview($0) }
        
        let whiteView = UIView(frame: initialFrame)
        whiteView.backgroundColor = fromVC.view.backgroundColor
        containerView.addSubview(whiteView)
        
        blurEffectView.frame = containerView.bounds
        containerView.addSubview(blurEffectView)
        blurEffectView.alpha = 0
        
        var headerFrame: CGRect = initialFrame
        headerFrame.origin = CGPoint(x: 0, y: 0)
        
        headerView.frame = headerFrame
        headerView.updateLayout(for: .presentation)
        headerView.layoutIfNeeded()
        
        let tmpContainerView = UIView(frame: initialFrame)
        tmpContainerView.backgroundColor = color
        tmpContainerView.addSubview(headerView)
        tmpContainerView.layer.cornerRadius = 16
        tmpContainerView.clipsToBounds = true
        containerView.addSubview(tmpContainerView)
        
        headerFrame.size = headerView.systemLayoutSizeFitting(CGSize(width: self.screenWidth, height: 0))
        
        headerView.heightAnchor.constraint(equalToConstant: headerFrame.size.height).isActive = true
        headerView.widthAnchor.constraint(equalToConstant: screenWidth).isActive = true
        
        headerView.translatesAutoresizingMaskIntoConstraints = false
        
        var containerFrame = containerView.frame
        containerFrame.size.width = screenWidth
        containerFrame.origin.x = (containerView.frame.width - screenWidth) / 2
        containerFrame.origin.y = topPadding
        
        UIView.animate(withDuration: transitionDuration, animations: {
            self.blurEffectView.alpha = 1
            headerView.frame = headerFrame
            headerView.layoutIfNeeded()
            
            tmpContainerView.frame = containerFrame
            tmpContainerView.layer.cornerRadius = 0
        }, completion: {
            tmpContainerView.removeFromSuperview()
            toVC.view.frame = tmpContainerView.frame
            toVC.view.clipsToBounds = false
            containerView.addSubview(toVC.view)
            
            completion($0)
        })

    }
    
    private func dismiss(fromVC: UIViewController, toVC: UIViewController, containerView: UIView, headerView: TitledPhotoView, initialFrame: CGRect, completion: @escaping (Bool) -> Void) {
        
    }
}

extension TransitionManager: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.transition = .presentation
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.transition = .dismissal
        return self
    }
}
