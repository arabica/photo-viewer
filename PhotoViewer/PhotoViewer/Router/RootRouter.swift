//
//  RootRouter.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/17/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import UIKit
import RxSwift

class RootRouter {
    enum State {
        case login(LoginViewController, LoginViewModel)
        case photoFeed
        case undetermined(UIViewController?)
    }
    
    private let disposeBag = DisposeBag()
    private let transitionManager = TransitionManager()
    let window: UIWindow
    private (set) var state: State
    
    var rootController: UIViewController? {
        get {
            return self.window.rootViewController
        }
        set {
            self.window.rootViewController = newValue
        }
    }
    
    init(window: UIWindow) {
        self.window = window
        self.state = .undetermined(window.rootViewController)
    }
    
    func setInitialState(authClient: AuthClient = AuthClient()) {
        switch authClient.state {
        case .logedIn:
            openPhotoFeed()
        case .notAuthorized:
            self.requestLogin()
                .subscribe { [weak self] (_) in
                    self?.openPhotoFeed()
            }
            .disposed(by: self.disposeBag)
        }
    }
    
    func openPhotoFeed() {
        let photoFeedViewModel = PhotoFeedViewModel()
        let photoFeedViewController = PhotoFeedViewController(viewModel: photoFeedViewModel)
        
        photoFeedViewModel.itemSelected
            .subscribe(
                onNext: { [unowned self] in
                    self.displayPhotoInfo(photo: $0.0, image: $0.1, frame: $0.2)
                }
            )
            .disposed(by: self.disposeBag)
        
        self.setRootController(photoFeedViewController)
    }
    
    func displayPhotoInfo(photo: FlickrPhoto, image: UIImage?, frame: CGRect?) {
        let viewModel = DetailedPhotoViewModel(photo: photo)
        let viewController = DetailedPhotoViewController(viewModel: viewModel, image: image)
        self.transitionManager.getPresentData = {
            return (image, photo.title, frame)
        }
        
        viewController.transitioningDelegate = self.transitionManager
        
        self.rootController?.present(viewController, animated: true, completion: nil)
        
    }
    
    func setRootController(_ viewController: UIViewController) {
        self.window.rootViewController = viewController
    }
    
    func requestLogin() -> Observable<Void> {
        let currentViewController = self.window.rootViewController
        let curentState = self.state
        let loginViewModel = LoginViewModel(authClient: AuthClient())
        let loginViewController = LoginViewController(viewModel: loginViewModel)
        
        self.rootController = loginViewController
        self.state = .login(loginViewController, loginViewModel)
        
        let logedIn = PublishSubject<Void>()
        
        loginViewModel.loginAction
            .observeOn(MainScheduler.asyncInstance)
            .do(
                onNext: { [weak self] (_) in
                    self?.rootController = currentViewController
                    self?.state = curentState
                }
            )
            .subscribe(logedIn)
            .disposed(by: self.disposeBag)
        
        return logedIn
    }
}
