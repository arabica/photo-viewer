//
//  PhotoCollectionViewCell.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/24/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet var title: UILabel!
    @IBOutlet var image: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 15
    }
    
}
