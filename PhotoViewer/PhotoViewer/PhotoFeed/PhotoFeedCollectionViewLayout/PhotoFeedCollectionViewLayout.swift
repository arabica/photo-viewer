//
//  PhotoFeedCollectionViewLayout.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/25/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import UIKit

class PhotoFeedCollectionViewLayout: UICollectionViewLayout {
    private let minimalColumnWidth: CGFloat = 300
    
    var numberOfColumns: Int {
        return Int((self.collectionView?.bounds.width ?? 0) / minimalColumnWidth)
    }
    
    var cellPadding: CGFloat = 8
    
    var cache = [UICollectionViewLayoutAttributes]()
    
    var heightForCellAtIndexPath : ((_ indexPath: IndexPath) -> CGSize?)?
    
    private var contentHeight: CGFloat = 0
    private var width: CGFloat {
        return self.collectionView?.bounds.width ?? 0
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: width, height: contentHeight)
    }
    
    override func invalidateLayout() {
        super.invalidateLayout()
        self.cache.removeAll()
    }
    
    override func prepare() {
        guard cache.isEmpty else { return }
        let columnWidth = self.width / CGFloat(self.numberOfColumns)
        
        var xOffsets = [CGFloat]()
        (0..<self.numberOfColumns).forEach { xOffsets.append(CGFloat($0) * columnWidth) }
        
        var yOffsets = [CGFloat](repeating: 0, count: self.numberOfColumns)
        var column = 0
        self.cache += (0..<(self.collectionView?.numberOfItems(inSection: 0) ?? 0))
            .map {
                let indexPath = IndexPath(item: $0, section: 0)
                
                let cellHeight = self.heightForCellAtIndexPath?(indexPath).map { (columnWidth / $0.width) * $0.height } ?? columnWidth
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = CGRect(x: xOffsets[column],
                                          y: yOffsets[column],
                                          width: columnWidth,
                                          height: cellHeight).insetBy(dx: self.cellPadding, dy: self.cellPadding)
                
                self.contentHeight = max(self.contentHeight, attributes.frame.maxY)
                yOffsets[column] = yOffsets[column] + cellHeight
                column = column >= self.numberOfColumns - 1 ? 0 : column + 1
                
                return attributes
            }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return self.cache.filter {
            $0.frame.intersects(rect)
        }
    }
    
}
