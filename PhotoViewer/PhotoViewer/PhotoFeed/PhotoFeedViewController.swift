//
//  PhotoFeedViewController.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/24/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import RxSwift
import UIKit
import AlamofireImage

class PhotoFeedViewController: UIViewController, UICollectionViewDelegate {
    private let uiActivityIndicator = ProgressHUD()
    
    @IBOutlet var collectionView: UICollectionView!
    
    let viewModel: PhotoFeedViewModel
    let disposeBag = DisposeBag()
    
    init(viewModel: PhotoFeedViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "PhotoFeedViewController", bundle: Bundle(for: type(of: self)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.uiActivityIndicator.trackOn(self.viewModel.activityIndicator)
        self.setupCollectionView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
    }
}

extension PhotoFeedViewController {
    private func setupCollectionView() {
        self.collectionView.register(PhotoCollectionViewCell.self)

        let layout = collectionView.collectionViewLayout as? PhotoFeedCollectionViewLayout
        layout?.heightForCellAtIndexPath = { indexPath in
            let photo = (try? self.viewModel.dataSource.value())?[indexPath.row]
            return photo.map { $0.size.flatMap { CGSize(width: $0.width, height: $0.height) } } ?? nil
        }
        
        self.viewModel
            .dataSource
            .observeOn(MainScheduler.asyncInstance)
            .catchError { [weak self] (error) -> Observable<[FlickrPhoto]> in
                let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self?.present(alertController, animated: true, completion: nil)
                return Observable.just([FlickrPhoto]())
            }
            .bind(to: self.collectionView.rx
                .items(cellIdentifier: PhotoCollectionViewCell.identifier, cellType: PhotoCollectionViewCell.self)) { row, data, cell in
                    cell.title.text = data.title
                    data.url.flatMap {
                        cell.image.af_setImage(withURL: $0)
                    }
            }.disposed(by: self.disposeBag)
        
        self.collectionView.rx.modelSelected(FlickrPhoto.self)
            .withLatestFrom(self.collectionView.rx.itemSelected) { ($0, $1) }
            .map { [unowned self] (photo, indexPath) in
                let cell = self.collectionView.cellForItem(at: indexPath)
                let image = (cell as? PhotoCollectionViewCell)?.image.image
                let attributes = (self.collectionView.collectionViewLayout as? PhotoFeedCollectionViewLayout)?.cache[indexPath.row]
                let cellFrame = (attributes?.frame).map { oldFrame -> CGRect in
                    var realFrame = oldFrame
                    realFrame.origin.y = oldFrame.origin.y - self.collectionView.contentOffset.y
                    return realFrame
                }
                
                return (photo, image, cellFrame)
            }
            .subscribe(self.viewModel.itemSelected)
            .disposed(by: self.disposeBag)
    }
}
