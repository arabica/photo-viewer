//
//  DetailedPhotoViewModel.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/25/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import RxSwift

class DetailedPhotoViewModel {
    let dataProvider: FlickrPhotoDataProvider
    let photo: FlickrPhoto
    
    let activityIndicator = ActivityIndicator()
    let errorTracker = ErrorTracker()
    
    init(photo: FlickrPhoto, dataProvider: FlickrPhotoDataProvider = FlickrPhotoDataProvider()) {
        self.dataProvider = dataProvider
        self.photo = photo
    }
    
    func getPhotoInfo() -> Observable<FlickrPhotoInfoModel> {
        return self.dataProvider
            .photoInfo(photo: self.photo)
            .trackActivity(self.activityIndicator)
            .trackError(self.errorTracker)
    }
    
    func getComments() -> Observable<[FlickrPhotoComment]> {
        return self.dataProvider
            .photoComments(self.photo)
            .trackActivity(self.activityIndicator)
            .trackError(self.errorTracker)
    }
}
