//
//  DetailedPhotoViewController.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/16/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import AlamofireImage
import UIImageColors

class DetailedPhotoViewController: UIViewController {
    private static let identifier = "DetailedPhotoViewController"
    private let image: UIImage?
    private let viewModel: DetailedPhotoViewModel
    private let disposeBag = DisposeBag()
    private let colors: UIImageColors
    private let uiActivityIndicator = ProgressHUD()
    
    @IBOutlet var tableView: UITableView!
    
    init(viewModel: DetailedPhotoViewModel, image: UIImage?) {
        self.image = image
        self.viewModel = viewModel
        self.colors = image?.getColors() ?? UIImageColors(background: .white, primary: .black, secondary: .black, detail: .gray)
        super.init(nibName: DetailedPhotoViewController.identifier, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableHeader(image: image, title: viewModel.photo.title)
        self.configureBindings()
        self.tableView.backgroundColor = self.colors.background
        self.view.backgroundColor = self.colors.background
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.updateHeaderViewSize()
    }
}

extension DetailedPhotoViewController {
    private func configureBindings() {
        self.uiActivityIndicator.trackOn(self.viewModel.activityIndicator)
        
        self.viewModel.getPhotoInfo()
            .observeOn(MainScheduler.asyncInstance)
            .debug()
            .subscribe(
                onNext: { [weak self] in
                    let headerView = self?.tableView.tableHeaderView as? TitledPhotoView
                    headerView?.userName.text = $0.owner.username
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateStyle = .short
                    dateFormatter.timeStyle = .short
                    headerView?.dateLabel.text = dateFormatter.string(from: $0.dateUploaded)
                    $0.owner.avatar.flatMap { headerView?.avatar.af_setImage(withURL: $0, placeholderImage: UIImage(named: "icons8-male-user")) }
                    headerView?.title.text = $0.title
                    $0.comments.flatMap { headerView?.commentBtn.setTitle("\($0)", for: .normal) }
                    $0.countFaves.flatMap { headerView?.favoriteBtn.setTitle("\($0)", for: .normal) }
                }
            ).disposed(by: self.disposeBag)
        
        self.tableView.register(PhotoCommentCell.self)
        self.viewModel
            .getComments()
            .debug()
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: self.tableView.rx.items(cellIdentifier: PhotoCommentCell.identifier, cellType: PhotoCommentCell.self)) { [weak self] _, data, cell in
                cell.userName.text = data.authorName
                cell.comment.text = data.content
                cell.avatar.af_setImage(withURL: data.avatar!, placeholderImage: UIImage(named: "icons8-male-user"))
                
                (self?.colors).flatMap {
                    cell.userName.textColor = $0.primary
                    cell.comment.textColor = $0.secondary
                }
            }
            .disposed(by: self.disposeBag)
    }
    
    private func updateHeaderViewSize() {
        guard let headerView = tableView.tableHeaderView as? TitledPhotoView else {
            return
        }
        
        let width = (self.transitioningDelegate as? TransitionManager)?.screenWidth ?? self.view.frame.width
        let size = headerView.systemLayoutSizeFitting(CGSize(width: width, height: 0))
        headerView.frame.size.height = size.height
        tableView.tableHeaderView = headerView
    }
    
    private func setupTableHeader(image: UIImage?, title: String?) {
        let headerView = TitledPhotoView.instance()
        headerView.setImage(image: image)
        headerView.title.text = title
        
        tableView.tableHeaderView = headerView
        
        [headerView.favoriteBtn, headerView.commentBtn, headerView.shareBtn]
            .forEach {
                $0?.makeImageTemplate().tintColor = self.colors.primary
                $0?.setTitleColor(self.colors.primary, for: .normal)
            }
        
        headerView.closeBtn.rx.tap
            .subscribe(
                onNext: { [weak self] _ in
                    self?.dismiss(animated: true, completion: nil)
                }
            ).disposed(by: self.disposeBag)
    }
    
    
}
