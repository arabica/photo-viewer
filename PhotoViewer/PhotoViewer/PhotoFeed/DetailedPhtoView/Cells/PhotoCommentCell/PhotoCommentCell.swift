//
//  PhotoCommentCell.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/26/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import UIKit

class PhotoCommentCell: UITableViewCell {
    @IBOutlet var avatar: UIImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var comment: UILabel!
    
}
