//
//  TitledPhotoView.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/26/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import UIKit
import UIImageColors

class TitledPhotoView: UIView, NibInstance {
    private let standardInfoHeight: CGFloat = 140
    
    @IBOutlet var image: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var userName: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var avatar: UIImageView!
    
    @IBOutlet var favoriteBtn: UIButton!
    @IBOutlet var commentBtn: UIButton!
    @IBOutlet var shareBtn: UIButton!
    
    @IBOutlet var closeBtn: UIButton!
    
    @IBOutlet var infoHeight: NSLayoutConstraint!
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
        var size = targetSize
        
        self.image.image.flatMap {
            size.height = size.width * $0.size.height / $0.size.width + self.infoHeight.constant
        }
        
        return size
    }
    
    func updateLayout(for transition: TransitionType) {
        switch transition {
        case .dismissal:
            infoHeight.constant = standardInfoHeight
        case .presentation:
            infoHeight.constant = 0
        }
    }
    
    func setImage(image: UIImage?) {
        self.image.image = image
        let multiplier: CGFloat = image.flatMap { $0.size.height / $0.size.width } ?? 1
        self.image.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: multiplier).isActive = true
        let imageColors = self.image.image!.getColors(quality: .low)
        self.title.textColor = imageColors.primary
        self.userName.textColor = imageColors.primary
        self.dateLabel.textColor = imageColors.detail
        self.favoriteBtn.tintColor = imageColors.primary
        self.shareBtn.tintColor = imageColors.primary
        self.commentBtn.tintColor = imageColors.primary
        
    }
    /*
    func sizeThatFits(_ width: CGFloat) -> CGSize {
        guard let image = self.image.image else {
            return CGSize(width: width, height: standardInfoHeight)
        }
        
        let size = CGSize(width: width, height: width * (image.size.height / image.size.width))
        return size
    }
 */
    
}
