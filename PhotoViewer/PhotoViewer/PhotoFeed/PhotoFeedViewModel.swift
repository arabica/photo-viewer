//
//  PhotoFeedViewModel.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/24/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import RxSwift

class PhotoFeedViewModel {
    private (set) var dataSource = BehaviorSubject<[FlickrPhoto]>(value: [])
    private let dataProvider: FlickrPhotoDataProvider
    private let disposeBag = DisposeBag()
    
    let activityIndicator = ActivityIndicator()
    let errorTracker = ErrorTracker()
    
    let itemSelected = PublishSubject<(FlickrPhoto, UIImage?, CGRect?)>()
    
    init(dataProvider: FlickrPhotoDataProvider = FlickrPhotoDataProvider()) {
        self.dataProvider = dataProvider
        self.configureBindings()
    }
}

extension PhotoFeedViewModel {
    private func configureBindings() {
        self.dataProvider.search()
            .withLatestFrom(self.dataSource) { $0 + $1 }
            .trackActivity(self.activityIndicator)
            .trackError(self.errorTracker)
            .subscribe(self.dataSource)
            .disposed(by: self.disposeBag)
    }
}
