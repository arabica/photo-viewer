//
//  ActivitiIndicator.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/27/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import Foundation
import RxSwift

private struct ActivityToken<E> : ObservableConvertibleType, Disposable {
    private let _source: Observable<E>
    private let _dispose: Cancelable
    
    init(source: Observable<E>, disposeAction: @escaping () -> ()) {
        _source = source
        _dispose = Disposables.create(with: disposeAction)
    }
    
    func dispose() {
        _dispose.dispose()
    }
    
    func asObservable() -> Observable<E> {
        return _source
    }
}

/**
 Enables monitoring of sequence computation.
 
 If there is at least one sequence computation in progress, `true` will be sent.
 When all activities complete `false` will be sent.
 */
class ActivityIndicator : ObservableConvertibleType {
    public typealias E = Bool
    
    private let queue = DispatchQueue(label: String(describing: ActivityIndicator.self) + "-" + UUID().uuidString)
    private let _loading = BehaviorSubject<Bool>(value: false)
    private var counter = 0
    
    init() {}
    
    fileprivate func trackActivityOfObservable<O: ObservableConvertibleType>(_ source: O) -> Observable<O.E> {
        return Observable.using({ () -> ActivityToken<O.E> in
            self.increment()
            return ActivityToken(source: source.asObservable(), disposeAction: self.decrement)
        }) { t in
            return t.asObservable()
        }
    }
    
    private func increment() {
        self.queue.sync() {
            self.counter += 1
            self._loading.onNext(self.counter > 0)
        }
    }
    
    private func decrement() {
        self.queue.sync() {
            assert(self.counter > 0)
            self.counter -= 1
            self._loading.onNext(self.counter > 0)
        }
    }
    
    func asObservable() -> Observable<Bool> {
        return _loading.asObserver()
    }
}

extension ObservableConvertibleType {
    func trackActivity(_ activityIndicator: ActivityIndicator) -> Observable<E> {
        return activityIndicator.trackActivityOfObservable(self)
    }
}
