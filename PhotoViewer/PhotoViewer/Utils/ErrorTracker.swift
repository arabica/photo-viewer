//
//  ErrorTracker.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/27/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import Foundation
import RxSwift

final class ErrorTracker: ObservableConvertibleType {
    private let _subject = PublishSubject<Error>()
    
    func trackError<O: ObservableConvertibleType>(from source: O) -> Observable<O.E> {
        return source.asObservable().do(onError: onError)
    }
    
    func asObservable() -> Observable<Error> {
        return _subject.asObservable()
    }
    
    func onError(_ error: Error) {
        _subject.onNext(error)
    }
    
    deinit {
        _subject.onCompleted()
    }
}

extension ObservableConvertibleType {
    func trackError(_ errorTracker: ErrorTracker) -> Observable<E> {
        return errorTracker.trackError(from: self)
    }
}
