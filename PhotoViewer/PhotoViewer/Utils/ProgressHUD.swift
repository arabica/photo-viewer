//
//  ProgressHUD.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/28/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import RxCocoa
import RxSwift
import SVProgressHUD

class ProgressHUD {
    private let disposeBag = DisposeBag()
    
    func trackOn(_ activityIndicator: ActivityIndicator) {
        activityIndicator
        .asObservable()
        .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { (isBusy) in
                if isBusy {
                    SVProgressHUD.show()
                } else {
                    SVProgressHUD.dismiss()
                }
            }
        )
        .disposed(by: self.disposeBag)
    }
}
