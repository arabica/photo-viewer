//
//  LoginViewModel.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/17/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import RxSwift

class LoginViewModel {
    let oauthClient: AuthClient
    let loginAction = PublishSubject<Void>()
    let activityIndicator = ActivityIndicator()
    
    init(authClient: AuthClient = AuthClient()) {
        self.oauthClient = authClient
    }
    
    func login() -> Observable<Void> {
        return self.oauthClient
            .authorize()
            .trackActivity(self.activityIndicator)
            .mapToVoid()
            .do(
                onNext: { [weak self] _ in
                    self?.loginAction.onNext(())
                }, onError: { [weak self] error in
                    self?.loginAction.onError(error)
                }, onCompleted: { [weak self] in
                    self?.loginAction.onCompleted()
                }
            )
    }
}
