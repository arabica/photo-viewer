//
//  LoginViewController.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/17/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class LoginViewController: UIViewController {
    private let disposeBag = DisposeBag()
    private let viewModel: LoginViewModel
    private let uiActivityIndicator = ProgressHUD()
    @IBOutlet private var loginBtn: UIButton!
    
    init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "LoginViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureBindings()
    }
}

extension LoginViewController {
    private func configureBindings() {
        self.uiActivityIndicator.trackOn(self.viewModel.activityIndicator)
        
        self.loginBtn.rx.tap.flatMap {
                self.viewModel.login()
            }
            .subscribe()
            .disposed(by: self.disposeBag)
    }
}
