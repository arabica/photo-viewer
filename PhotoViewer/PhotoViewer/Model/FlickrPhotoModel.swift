//
//  FlickrPhotoModel.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/23/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import Foundation

struct FlickerPhotoSearchResponse: FlickrResponse {
    struct Photos: Codable {
        var page: Int?
        var pages: Int?
        var perpage: Int?
        var total: String?
        var photo: [FlickrPhoto]?
    }
    
    var stat: FlickrResponseStatus
    var message: String?
    var photos: Photos
}

struct FlickrPhoto: Codable, Equatable {
    var identity: String {
        return self.id
    }
    
    typealias Identity = String
    
    static func == (lhs: FlickrPhoto, rhs: FlickrPhoto) -> Bool {
        return lhs.id == rhs.id 
    }
    
    enum CodingKeys: String, CodingKey {
        case id, owner, secret, server, farm, title
        case width = "o_width"
        case height = "o_height"
    }
    
    struct Size {
        let width, height: Int
    }
    
    let id: String
    let owner: String?
    let secret: String
    let server: String
    let farm: Int
    let title: String?
    let size: Size?
    
    var url: URL? {
        return URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret).jpg")
    }
    
    func encode(to encoder: Encoder) throws { }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try values.decode(String.self, forKey: .id)
        self.owner = try? values.decode(String.self, forKey: .owner)
        self.title = try? values.decode(String.self, forKey: .title)
        
        self.farm = try values.decode(Int.self, forKey: .farm)
        self.server = try values.decode(String.self, forKey: .server)
        self.secret = try values.decode(String.self, forKey: .secret)
        
        if let widthStr = try? values.decode(String.self, forKey: .width),
            let heightStr = try? values.decode(String.self, forKey: .height),
            let width = Int(widthStr), let height = Int(heightStr) {
            self.size = Size(width: width, height: height)
        } else {
            self.size = nil
        }
    }
    
}
