//
//  FlickrPhotoInfoModel.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/26/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import Foundation

struct FlickrPhotoInfoResponse: FlickrResponse {
    var stat: FlickrResponseStatus
    var message: String?
    let photo: FlickrPhotoInfoModel
}

struct FlickrPhotoInfoModel: Codable {
    
    enum Errors: Error {
        case codableError
    }
    
    private struct Urls: Codable {
        struct Url: Codable {
            let type: String
            let _content: String
        }
        let url: [Url]
    }
    
    enum CodingKeys: String, CodingKey {
        case dateuploaded, isfavorite, owner, title, description, urls, comments, views, count_faves
    }
    
    let dateUploaded: Date
    let isFavorite: Bool
    let owner: User
    let pageUrl: URL?
    let title: String
    let description: String?
    let viewes: Int?
    let comments: Int?
    let countFaves: Int?
    
    func encode(to encoder: Encoder) throws { }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        let uploaded = try values.decode(String.self, forKey: .dateuploaded)
        guard let timeInterval = Int(uploaded) else {
            throw Errors.codableError
        }
        self.dateUploaded = Date(timeIntervalSince1970: TimeInterval(timeInterval))
        
        let favorite = try values.decode(Int.self, forKey: .isfavorite)
        self.isFavorite = favorite != 0
        
        self.owner = try values.decode(User.self, forKey: .owner)
        
        let url = try? values.decode(Urls.self, forKey: .urls)
        self.pageUrl = (url?.url.filter { $0.type == "photopage" }.first?._content ?? url?.url.first?._content).flatMap { URL(string: $0) }
        
        self.title = try values.decode(FlickrContent.self, forKey: .title).content
        self.description = try values.decode(FlickrContent.self, forKey: .description).content
        
        let commentsStr = try? values.decode(FlickrContent.self, forKey: .comments).content
        self.comments = commentsStr.flatMap { Int($0) }
        
        let viewsStr = try? values.decode(String.self, forKey: .views)
        self.viewes = viewsStr.flatMap { Int($0) }
        
        let countFavesStr = try? values.decode(String.self, forKey: .count_faves)
        self.countFaves = countFavesStr.flatMap { Int($0) }
    }
}
