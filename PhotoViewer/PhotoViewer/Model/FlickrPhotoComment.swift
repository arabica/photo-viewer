//
//  FlickrPhotoComment.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/26/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import Foundation

struct FlickrPhotoCommentResponse: FlickrResponse {
    
    struct Comments: Codable {
        let comment: [FlickrPhotoComment]?
    }
    
    let stat: FlickrResponseStatus
    let message: String?
    let comments: Comments
}

struct FlickrPhotoComment: Codable {
    enum CodingKeys: String, CodingKey {
        case id, author
        case authorName = "authorname"
        case iconServer = "iconserver"
        case iconFarm = "iconfarm"
        case content = "_content"
    }
    
    let id: String
    let author: String
    let authorName: String
    let iconServer: String
    let iconFarm: Int
    let content: String
    
    var avatar: URL? {
        if (Int(iconServer) ?? 0) > 0 {
            return URL(string: "http://farm\(iconFarm).staticflickr.com/\(iconServer)/buddyicons/\(author).jpg")
        } else {
            return URL(string: "https://www.flickr.com/images/buddyicon.gif")
        }
    }
}
