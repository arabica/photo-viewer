//
//  User.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/26/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import Foundation

struct User: Codable {
    let nsid: String
    let username: String
    let realname: String
    let iconserver: String
    let iconfarm: Int
    
    var avatar: URL? {
        if (Int(iconserver) ?? 0) > 0 {
            return URL(string: "http://farm\(iconfarm).staticflickr.com/\(iconserver)/buddyicons/\(nsid).jpg")
        } else {
            return URL(string: "https://www.flickr.com/images/buddyicon.gif")
        }
    }
}
