//
//  FlickrResponse.swift
//  PhotoViewer
//
//  Created by Mykola Kibysh on 12/23/18.
//  Copyright © 2018 Nick Kibish. All rights reserved.
//

import Foundation

enum FlickrResponseStatus: String, Codable {
    case ok, fail
}

protocol FlickrResponse: Codable {
    var stat: FlickrResponseStatus { get }
    var message: String? { get }
}

struct FlickrContent: Codable {
    let content: String
    
    enum CodingKeys: String, CodingKey {
        case content = "_content"
    }
}
