# Features

Here's a simple Photo Viewer to show photos from [Flickr](https://www.flickr.com). 

The application consists from 3 screens

**Login screen**: screen with one button which opens Flicker for authorization. Auth process is required. You should have Flicker account or just use test credentials: `testuser653@yahoo.com` / `pass-api-123`

**Photo feed:** This is collection view which displays your most relevant photos.

**Photo details:** That screen displays photo in native dimension. It also shows number of likes and comments. Here also you can see list of comments. 

# Dependencies

Here're libraries I used in the development process:

- **[Alamofire](https://github.com/Alamofire/Alamofire):** great library for HTTP requests
- **[AlamofireImage](https://github.com/Alamofire/AlamofireImage):** Library based on Alamofire for downloading/uploading images
- **[CodableAlamofire](https://github.com/Otbivnoe/CodableAlamofire):** extension for Alamofire which allows encode/decode objects easily.
- **[RxSwift / RxCocoa](https://github.com/ReactiveX/RxSwift):** well-known libraries for which bring [FRP](https://en.wikipedia.org/wiki/Functional_reactive_programming) to Swift
- **[KeychainSwift](https://github.com/evgenyneu/keychain-swift)[:](KeychainSwift)** Wrapper around Security framework and Keychain. I use it to store tokens secure.
- **[OAuthSwift](https://github.com/OAuthSwift/OAuthSwift):** Flickr uses OAuth1 protocol for authorization. This library covers all stages of authorization process.
- **[SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD):** easy-to-use HUD
- **[UIImageColors](https://github.com/jathu/UIImageColors):** iTunes style color fetcher for UIImage. It fetches the most dominant and prominent colors. Looks great but has problems with performance. I'd preferred to provide more optimized code based on Accelerate framework or GPU-based code (maybe something using histograms)

# Features I spent most of the time on

I spent around 40 hours to build the app and about 20 hours to understand the Flickr's authentication system. 

1. **API Client**. I spent a lot to get the Flicker auth system. As a result API layer took most of my time. Finally I've created all necessary components for working with Flickr API. 

    Flicker API layer is initialized with AuthClient which provides required Parameter Encoder which build correct URLRequest. Parameter Encoder is created by AuthClient. It takes access token and sign the request using SHA-1 and add the signature to each request.

2. **Custom transition.** Working with UI always takes some time. Especially if it's custom UI. I implemented custom transition from photo feed to Photo details screen. There are a lot of calculations in transitioning section (TransitionManager.swift) to get correct positions of views. However even now it's not as smooth as I wished. 
![Custom present](https://bitbucket.org/arabica/photo-viewer/raw/f2a323a47471c66d1ebfbfff267e92dd7ca2c5f7/screenshots/custom-present.gif)
3. **Mosaic collection view layout.** That didn't take much time. That's just looks fine. It divide screen on several columns based on screen width. 
![iPad Landscape mosaic view](https://bitbucket.org/arabica/photo-viewer/raw/f2a323a47471c66d1ebfbfff267e92dd7ca2c5f7/screenshots/ipad-landscape.jpeg) ![iPad Portrait Mosaic view](https://bitbucket.org/arabica/photo-viewer/raw/f2a323a47471c66d1ebfbfff267e92dd7ca2c5f7/screenshots/ipad-portrait.jpeg) ![iPhone Mosaic view](https://bitbucket.org/arabica/photo-viewer/raw/f2a323a47471c66d1ebfbfff267e92dd7ca2c5f7/screenshots/iphone-portrait.jpeg)

# Features I wish to improve

1. **Custom transitioning.** That looks OK however it's not smooth. There are some UI defects on transitioning. Owing to custom transition Detail screen rotation breaks the UI. I've also provided only presenting. There's no custom dismissing. Moreover there are huge methods. I guess the number of code lines could be reduced. 
2. **Colorized background and text.** I use third-party library to get custom colors for each screen based on image. However it has bad performance. I fond that lib on GitHub and didn't have much time to familiar with it. But I'd wish to provide more optimized code based on algorithms from Accelerate framework or some code which uses power of GPU.
3. **Unit / Integration tests.** I didn't cover code with any tests. But it's important part of the development process. 
4. **Rx code.** Reactive programming is great paradigm. However I believe it's revealed on bigger projects. Rx code can be looked as confused but it provides great flexibility and scalability. 

# How to run the project

1. Go to project directory
2. Run `pod install`.  If you don't have CocoaPods installed run `sudo gem install cocoapods`.
3. Use `testuser653@yahoo.com / pass-api-123` or any yahoo user you want to login to the app.